FROM ubuntu:18.04

ENV DEBIAN_FRONTEND noninteractive

# Get the dependencies
RUN apt-get update \
 && apt-get install -y \
    git g++ cmake \
    libsqlite3-dev \
    libwxgtk3.0-dev freeglut3-dev \
    libusb-1.0-0-dev \
    libi2c-dev

# Install SoapySDR
RUN apt-get install -y libpython-dev python-numpy swig \
 && git clone https://github.com/pothosware/SoapySDR.git -b soapy-sdr-0.7.2 \
 && cd SoapySDR \
 && mkdir build \
 && cd build \
 && cmake .. \
 && make -j4 \
 && make install \
 && ldconfig

# Install LimeSuite
RUN git clone https://github.com/myriadrf/LimeSuite.git -b v20.10.0\
 && cd LimeSuite \
 && mkdir builddir && cd builddir \
 && cmake ../ \
 && make -j4 \
 && make install \
 && ldconfig \
 && cd ../udev-rules \
 && ./install.sh

# Fetch configurations for LimeSDR
RUN mkdir -p /etc/limemicro && cd /etc/limemicro \
 && git clone https://github.com/myriadrf/trx-lms7002m
